#pragma once

// types of common "user" supplied functions

// a functional on a finite dimensional space (i.e. a scalar valued function)
typedef dfloat fnl_fd(int n, dfloat *x, void *data);
