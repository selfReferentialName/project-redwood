/** @file util/log.h
 * @brief Log various events.
 */
#ifndef UTIL_LOG_H
#define UTIL_LOG_H

// Loging stuff below this level should be optimised out.
#ifndef NDEBUG
#define LOG_MAX_LEVEL LOG_VERY_VERBOSE
#else
#define LOG_MAX_LEVEL LOG_VERBOSE
#endif

/// The verbosity of log messages.
enum log_level {
	LOG_ALWAYS, ///< Messages which can't be turned off (e.g. crash info)
	LOG_ERROR, ///< Recoverable error mesages (e.g. texture not found)
	LOG_WARN, ///< Warnings
	LOG_DEFAULT, ///< Very major events which should usually be logged
	LOG_EVENTS, ///< Major events which don't always need to be logged
	LOG_VERBOSE, ///< Minor events which can be helpful when debugging
	LOG_VERY_VERBOSE ///< Extremely verbose logging. Disabled in release builds.
};

#include <stdlib.h>
#include <errno.h>

/** @brief Log a string like puts.
 *
 * Log a string at a given log level. Automatically appends a newline.
 *
 * @param msg The message to log.
 * @param level The verbosity of the message.
 *
 * @see logfmt
 */
#define logstr(msg, level) \
	do { \
		if (level <= LOG_MAX_LEVEL) { \
			log_raw(msg, level, __FILE__, __LINE__); \
		} \
	} while (0)

/** @brief Log with formatting like printf.
 *
 * Log a formatted message at a given log level. The message format
 * is identical to that of printf. Appends a newline.
 *
 * @param level The verbosity of the message.
 * @param fmt The message format.
 *
 * @see logstr
 */
#define logfmt(level, fmt, ...) \
	do { \
		if (level <= LOG_MAX_LEVEL) { \
			logf_raw(level, __FILE__, __LINE__, fmt, __VA_ARGS__); \
		} \
	} while (0)

/** @brief Assert an expression.
 *
 * Acts like C's standard function assert, but calls logstr on failure.
 *
 * @param e The expression which must be nonzero.
 *
 * @see log_unreachable log_todo
 */
#define log_assert(e) \
	do { \
		if (!(e)) { \
			int log_errno = errno; \
			logstr("Assertation failure: " #e, LOG_ALWAYS); \
			errno = log_errno; \
			log_errnos(); \
			exit(1); \
		} \
	} while (0)

/** @brief Assert an expression with formatting.
 *
 * Acts like C's standard function assert, but calls logfmt on failure.
 *
 * @param e The expression which must be nonzero.
 * @param 
 *
 * @see log_unreachable log_todo
 */
#define logfmt_assert(e, msg, ...) \
	do { \
		if (!(e)) { \
			int log_errno = errno; \
			logfmt(LOG_ALWAYS, "Assertation failure: " #e ": " msg, __VA_ARGS__); \
			errno = log_errno; \
			log_errnos(); \
			exit(1); \
		} \
	} while (0)

/** @brief Complain that code is unreachable.
 *
 * Essentially the same thing as log_assert(false).
 *
 * @see log_assert log_todo
 */
#define log_unreachable() \
	do { \
		logstr("Unreachable code reached.", LOG_ALWAYS); \
		exit(1); \
	} while(0)

/** @brief Complain that code needs implementing.
 *
 * Just a fancy log_unreachable
 *
 * @see log_assert log_unreachable
 */
#define log_todo(msg) \
	do { \
		logstr("Code path is TODO: " msg, LOG_ALWAYS); \
		exit(1); \
	} while(0)

/** @brief Print a log message before running something.
 *
 * @see logs
 */
#define log_checkpoint(e, level) \
	do { \
		logstr(#e, level); \
		e; \
	} while(0)

// log is a macro which calls this function
void log_raw(const char *msg, enum log_level level, const char *file, int line);

// logf is a macro which calls this function
void logf_raw(enum log_level level, const char *file, int line, const char *fmt, ...);

/// Log all errno like variables if they're not success.
void log_errnos(void);

#endif
