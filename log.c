#include "log.h"

#include <stdarg.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

// + 1 for useless null terminator
static const char level_char[LOG_MAX_LEVEL + 1] = "!EW+-Vv";

void log_raw(const char *msg, enum log_level level, const char *file, int line)
{
	printf("[%c] %s at %s:%d\n", level_char[level], msg, file, line);
}

void logf_raw(enum log_level level, const char *file, int line, const char *fmt, ...)
{
	printf("[%c] ", level_char[level]);
	va_list args;
	va_start(args, fmt);
	vprintf(fmt, args);
	printf(" at %s:%d\n", file, line);
}

void log_errnos(void)
{
	if (errno > 0) {
		logfmt(LOG_ALWAYS, "errno: %s\n", strerror(errno));
	}
}
