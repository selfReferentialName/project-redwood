#include "control.h"
#include "log.h"
#include <math.h>
#include <nlopt.h>
#include <string.h>

#define nlopt_assert(e, tmpname) \
	do { \
		nlopt_result tmpname = e; \
		logfmt(LOG_VERY_VERBOSE, "NLopt call " #e " returned code %d", tmpname); \
		if (tmpname < 0) { \
			logfmt(LOG_ALWAYS, "NLopt call " #e " failed with code %d", tmpname); \
			exit(1); \
		} else if (tmpname >= 5) { \
			logfmt(LOG_ERROR, "NLopt call " #e " ran past limit (code %d)", tmpname); \
		} \
	} while (0)

void chebyshev(int n, dfloat t, dfloat *x, dfloat *dxdt)
{
	if (n > 0) x[0] = 1;
	if (n > 1) x[1] = t;
	for (int i=2; i<n; i++) {
		x[i] = 2*x[i-1] - x[i-2];
	}
	if (dxdt) {
		dfloat Un = 1;
		if (n > 0) dxdt[0] = 0;
		for (int i=1; i<n; i++) {
			dxdt[i] = i*Un;
			Un = t*Un + x[i];
		}
	}
}

void laguerre(int n, dfloat t, dfloat *x, dfloat *dxdt)
{
	if (n > 0) x[0] = 1;
	if (n > 1) x[1] = 1-t;
	for (int i=2; i<n; i++) {
		x[i] = ((2*i-1-t)*x[i-1] - (i-1)*x[i-2]) / (i);
	}
	if (dxdt) {
		for (int i=0; i<n; i++) {
			if (fabs(t) < 1e-15 || i==0) {
				dxdt[i] = -i;
			} else {
				dxdt[i] = (i*x[i]-i*x[i-1]) / t;
			}
		}
	}
}

static void reset_state(struct ode *self)
{
	memcpy(self->state, self->initstate, self->n*sizeof(dfloat));
	if (self->next_t) reset_state(self->next_t);
	if (self->next_w) reset_state(self->next_w);
	if (self->next_f) reset_state(self->next_f);
}

struct collocation_data {
	void *data;
	void *odedat;
	int m, k;
	dfloat *coeffs;
	ctrlbasis *basis;
	dfloat *basis_out;
	dfloat *basis_deriv;
	dfloat *inputs;
	dfloat *input_out;
	odef *basesys;
	vecvecfun *input;
	void *inputdata;
	dfloat tscale;
};

static void collocation_odef(int n, dfloat t, dfloat state[n], dfloat deriv[n], void *raw_data)
{
	struct collocation_data *self = raw_data;
//	logfmt(LOG_VERBOSE, "coeffs = %p", self->coeffs);
	self->basesys(n, t, state, deriv, self->odedat);
	self->basis(self->k, t, self->basis_out, self->basis_deriv);
#pragma omp parallel for
	for (int i=0; i<self->m; i++) {
		self->inputs[i] = 0;
		for (int j=0; j<self->k; j++) {
//			logfmt(LOG_VERBOSE, "[%d]: %g += %g * %g", i, self->inputs[i], self->coeffs[i+self->m*j], self->basis_out[j]);
			self->inputs[i] += self->coeffs[i+self->m*j] * self->basis_out[j];
		}
	}
	memset(self->input_out, 0, n*sizeof(dfloat));
	self->input(n, state, self->m, self->inputs, n, self->input_out, self->inputdata);
#pragma omp parallel for
	for (int i=0; i<n; i++) {
//		logfmt(LOG_VERBOSE, "[%d] += %g", i, self->input_out[i]);
		deriv[i] += self->input_out[i];
	}
}

static void openloopsim_odef(int n, dfloat t, dfloat state[n], dfloat deriv[n], void *data)
{
	struct openloop *self = data;
	self->base->f(n, t, state, deriv, self->base->data);
	int k = self->_collocation.k;
	self->_collocation.basis(k, t, self->_btmp, NULL);
	dfloat inputs[self->m];
	for (int i=0; i<self->m; i++) {
		inputs[i] = 0;
		for (int j=0; j<k; j++) {
			inputs[i] += self->_collocation.coldat[i+self->m*j] * self->_btmp[j];
		}
	}
	memset(self->_atmp, 0, n*sizeof(dfloat));
	self->input(n, state, self->m, inputs, n, self->_atmp, self->inputdata);
	for (int i=0; i<n; i++) {
		deriv[i] += self->_atmp[i];
	}
}

static double cost_measure(int n, dfloat *state, void *data)
{
	struct optctrl_options *self = data;
	dfloat input_cost = 0, state_cost = 0, input_deriv_cost = 0;
	if (self->input_cost) {
		// TODO: make this know if collocation or something else
		struct collocation_data *cld = self->_input_adjoined.data;
		input_cost = self->input_cost(self->system.m, cld->input_out, n, state, self->data_input_cost);
	}
	if (self->state_cost) {
		state_cost = self->state_cost(n, state, self->data_state_cost);
	}
	if (self->input_deriv_cost) {
		struct collocation_data *cld = self->_input_adjoined.data;
		log_assert(cld->basis_deriv);
		dfloat idv[self->system.m];
//		logfmt(LOG_VERBOSE, "%p", cld->coeffs);
		for (int i=0; i<self->system.m; i++) {
			idv[i] = 0;
			for (int j=0; j<self->colocation.k; j++) {
				idv[i] += cld->coeffs[i+j*self->system.m] * cld->basis_deriv[j];
//				logfmt(LOG_VERBOSE, "idv[%d] += %g * %g", i, cld->coeffs[i+j*self->system.m], cld->basis_deriv[j]);
			}
		}
		input_deriv_cost = self->input_deriv_cost(self->system.m, idv, self->data_input_deriv_cost);
	}
	if (input_cost==0 && state_cost==0 && input_deriv_cost==0) {
		logstr("This shouldn't happen.\n", LOG_ERROR);
		return 0;
	}
//	logfmt(LOG_VERBOSE, "%g,%g,%g", input_cost, state_cost, input_deriv_cost);
	return input_cost + state_cost + input_deriv_cost;
}

static double collocation_objective(unsigned n, const double *x, double *grad, void *data)
{
	log_assert(grad == NULL);
	struct optctrl_options *pself = data;
	struct optctrl_options self = *pself;
	struct collocation_data coldat = *(struct collocation_data*)self._input_adjoined.data;
	self._input_adjoined.data = &coldat;
	coldat.coeffs = x;
//	logfmt(LOG_VERBOSE, "%p", coldat.coeffs);
	dfloat running_cost = 0;
	self._input_adjoined.state = malloc(sizeof(dfloat) * n);
	reset_state(&self._input_adjoined);
	self._input_adjoined.eval(&self._input_adjoined, self.time_horizon,
			0, 0, 0, NULL, NULL,
			(self.input_cost||self.state_cost||self.input_deriv_cost)?cost_measure:NULL, &running_cost, &self);
	double goal_cost = self.goal(n, self._input_adjoined.state, self.data_goal);
	double cost = running_cost + goal_cost;
//	logfmt(LOG_VERY_VERBOSE, "current cost %g, integrated %g, goal %g", cost, running_cost, goal_cost);
	free(self._input_adjoined.state);
	return cost;
}

static void collocation(struct optctrl_options *opts, struct openloop *out)
{
	logfmt(LOG_VERBOSE, "optimising control for ode %p", &opts->system.ode);
	nlopt_algorithm alg;
	switch (opts->alg) {
	case OCA_C_COBYLA:
		alg = NLOPT_LN_NELDERMEAD;
		break;
	case OCA_C_DIRECT:
		alg = NLOPT_GN_DIRECT;
		break;
	default:
		log_unreachable();
	}
	nlopt_opt nlopt = nlopt_create(alg, opts->colocation.k*opts->system.m);
	log_assert(nlopt);
	nlopt_assert(nlopt_set_min_objective(nlopt, collocation_objective, opts), result);
	nlopt_assert(nlopt_set_initial_step1(nlopt, 0.1), result);
	if (opts->ctol) nlopt_assert(nlopt_set_ftol_abs(nlopt, opts->ctol), result);
	if (opts->xtol) nlopt_assert(nlopt_set_xtol_abs1(nlopt, opts->xtol), result);
	if (opts->maxtime) nlopt_assert(nlopt_set_maxtime(nlopt, opts->maxtime), result);
	nlopt_assert(nlopt_set_maxeval(nlopt, 10000), result);
	log_assert(opts->ctol || opts->xtol || opts->maxtime);

	dfloat *input_min = malloc(opts->colocation.k*opts->system.m * sizeof(dfloat));
	dfloat *input_max = malloc(opts->colocation.k*opts->system.m * sizeof(dfloat));
	for (int i=0; i<opts->colocation.k; i++) {
		for (int j=0; j<opts->system.m; j++) {
			input_min[j+i*opts->system.m] = opts->input_min[j];
			input_max[j+i*opts->system.m] = opts->input_min[j];
		}
	}
	// these lines break NLopt
	// they only act as regularisation if inputs are angles
	// (i.e. they restrict to only one solution)
//	if (opts->input_min) nlopt_assert(nlopt_set_lower_bounds(nlopt, input_min), result);
//	if (opts->input_max) nlopt_assert(nlopt_set_upper_bounds(nlopt, input_max), result);

	opts->_input_adjoined = opts->system.ode;
//	logfmt(LOG_VERY_VERBOSE, "opts->_input_adjoined = %p", &opts->_input_adjoined);
	struct collocation_data coldat = {
		.data = opts->system.data,
		.odedat = opts->system.ode.data,
		.m = opts->system.m,
		.k = opts->colocation.k,
		.coeffs = calloc(sizeof(dfloat), opts->colocation.k*opts->system.m),
		.basis = opts->colocation.basis,
		.basis_out = malloc(sizeof(dfloat) * opts->colocation.k),
		.inputs = malloc(sizeof(dfloat) * opts->system.m),
		.input_out = malloc(sizeof(dfloat) * opts->system.ode.n),
		.basesys = opts->system.ode.f,
		.input = opts->system.in,
		.inputdata = opts->system.data,
		.tscale = 1,
	};
	opts->_input_adjoined.data = &coldat;
	opts->_input_adjoined.f = &collocation_odef;
	opts->_input_adjoined.maxh = opts->time_horizon / (opts->colocation.k+1);
	if (opts->input_deriv_cost) coldat.basis_deriv = malloc(sizeof(dfloat) * opts->colocation.k);

	dfloat finalval;
	logstr("Beginning optimal control computation.", LOG_DEFAULT);
	nlopt_assert(nlopt_optimize(nlopt, coldat.coeffs, &finalval), result);
	logfmt(LOG_DEFAULT, "Optimal control completed with final cost %g.", finalval);

	out->_collocation.basis = opts->colocation.basis;
	out->_collocation.coldat = malloc(sizeof(dfloat)*opts->colocation.k*opts->system.m);
	log_assert(out->_collocation.coldat);
	memcpy(out->_collocation.coldat, coldat.coeffs, sizeof(dfloat)*opts->colocation.k*opts->system.m);
	out->_collocation.k = opts->colocation.k;
	out->m = opts->system.m;
	out->base = &opts->system.ode;
	log_assert(out->_atmp = malloc(sizeof(dfloat)*opts->system.ode.n));
	log_assert(out->_btmp = malloc(sizeof(dfloat)*opts->colocation.k));
	out->input = opts->system.in;
	out->inputdata = opts->system.data;

	nlopt_destroy(nlopt);
}

void calc_optctrl(struct optctrl_options *opts, struct openloop *out)
{
	collocation(opts, out);
}

void openloopsim(struct openloop *self, struct ode *out)
{
	*out = *self->base;
	out->f = openloopsim_odef;
	out->data = self;
}
