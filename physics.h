#pragma once

#include "config.h"
#include "common.h"
#include <jansson.h>
#include <stdbool.h>

// a vector field for an order 1 ODE
// state' = deriv, where ode1f sets deriv
typedef void odef(int n, dfloat t, dfloat state[n], dfloat deriv[n], void *data);

// first order ODE with all context needed to solve
struct ode {
	// evaluate self for time t
	// optionally sample, with samples put in the vector *smpv, allocated by eval, with length *smpc
	// sampling is done when either time has advanced by smp_t
	// or when the state has relatively changed in weighted norm by over smp_r
	// or when the state has changed by over smp_a
	// time is added at the start of the sampled vector
	// optionally integrate mf over the solution, returning measure, passing in mfdata
	// dxdt, if present, will be set to the final derivative wrt time
	void (*eval)(struct ode *self, dfloat t,
			dfloat smp_t, dfloat smp_r, dfloat smp_a, int *smpc, dfloat **smpv,
			fnl_fd *mf, dfloat *measure, void *mfdata);

	// data
	int n; // dimension of the problem
	odef *f; // defining function; x' = f(x)
	void *data; // passed to f
	dfloat *state; // set to initial state, returns final state
	dfloat *initstate; // kept at initial state, used for control purposes
	int state_disc_count; // number of state elements to change from last ode
	int *state_discs; // indices of the discontinuities to change
	dfloat tnext; // time until the next ODE model
	struct ode *next_t; // go to this after tnext time
	dfloat *wmnext, wnext; // inner product matrix to go to next model when ||state|| >= wnext
	struct ode *next_w; // go to this when wnext conditions met
	fnl_fd *fnext; // function to go to next model when fnext(state) == 0
	void *fnext_data; // passed to fnext
	struct ode *next_f; // go to this when fnext condition met
	dfloat *errwts; // how important each component is for error, NULL for all 1s
	dfloat rtol, atol; // relative and absolute weighted 2-norm tolerances
	dfloat tscale; // expected phase timespan; set to 0 to get a guess filled in
	dfloat maxh; // maximum step size
	bool controlled; // control will only effect this stage if true

	// internal stuff that may change, use ZII principles
	struct _generic_SUNMatrix *_wmnext; // SUNMatrix of the wm weights
	struct _generic_N_Vector *_tmpvecn; // a temporary vector of size n
	dfloat _smp_a, _smp_r; // copies of the smp_* arguments of eval
	struct _generic_N_Vector *_smp_last_vec; // last vector for sampling
	char _root_cause; // 'w' for wnext, 'a' for smp_a, 'r' for smp_r
};

extern struct ode ode_skel; // good defaults for an ode struct
extern struct ode ode_done; // an ode which does absolutely nothing on eval

// multi use bits of state
// when state is put together, it is put in ascending order here (except negative goes last)
// negative values mean a custom section of length -x
enum state_section {
	STS_INERTIAL, // 6 dfloats representing 3d position (m) and velocity (m/s), widely used
	STS_MASS, // 1 dfloat representing mass (kg)
	NUM_STATE_SECTIONS
};
extern int state_section_len[NUM_STATE_SECTIONS]; // length of corresponding state section

// a physical law function, adding something to the odef
typedef void phys_law_f(dfloat t, dfloat *affected, dfloat *extra, dfloat *deriv, void *data);

// a phisical law which may be combined into an odef
struct phys_law {
	phys_law_f *f; // the function defining the law
	int affected; // the section we put a derivative in
	int extrac; // number of additional sections
	int *extrav; // indices of additional sections, in the order they will be put in extra
	void *data; // passed to f

	dfloat *_extra; // backing memory for extra; set to NULL when constructing
};

// a collection of physical laws
struct phys_law_collection {
	struct phys_law *lawv; // the laws
	int *secv; // the sections used in total, sorted
	int lawc; // count of laws
	int secc; // count of sections

	dfloat *_deriv; // backing memory for deriv of the laws, set to NULL when constructing
};

// do the neccesary setup for stuff like secv of phys_law_collection and many fields of ode
void phys_laws_setup(struct phys_law_collection *laws, struct ode *ode);

odef run_phys_laws; // run a phys_law_collection

// data associated with a planet
struct planet_data {
	dfloat mu; // standard gravitational parameter (m^3/s^2); 3.9860044188e14 for Earth
};

// data for a planet with an interesting atmosphere
struct atmosphere_data {
	struct planet_data base;
	dfloat radius; // equitorial radius (m); 6.371e6 for Earth
	dfloat specgas; // universal gas constant over molar mass of air (m^2/s^2K); 287.052874247 for SSL conditions
	dfloat adiabic; // heat capacity ratio; 1.4 for earth (should be called adiabatic)
	int layers; // number of layers where tempurature is linear; 7 for the ISA standard atmosphere
	float *layer_alt; // altitude at layer start (m); -611 for the troposphere
	float *base_tmp; // temperature at layer start (K); 292.15 for the troposphere
	float *delta_tmp; // change in temperature by geopotential altitude (K/m or *C/m); -6.5e-3 for troposphere
	float *base_prs; // pressure at layer start (Pa); 108900 for the troposphere
};

// data for a stage
struct stage_data {
	dfloat cd0; // coefficient of drag at low speed
	dfloat cd1; // coefficient of drag at mach 1
	dfloat thrust; // engine thrust (N) or 0 for unpowered
	dfloat isp; // engine specific impulse (s); must never be negative (if thrust != 0)
	float Mcr; // critical mach number
	float area; // reference area for drag calculations
};

// data for an atmospheric simulations
struct atmosim_data {
	struct atmosphere_data atm;
	struct stage_data stage;
};

// physical law skeletons
extern const struct phys_law atmosim; // drag and other atmospheric effects (atmosim_data)
extern const struct phys_law oneplanet; // one planet's gravity (planet_data)
extern const struct phys_law inertial_fix; // second order ODE stuff (data unused)
extern const struct phys_law posigrade_thrust; // burn an engine in the direction of the velocity (stage_data)
extern const struct phys_law burn_mass_fix; // should be included whenever burning with no input (stage_data)

struct ode *parse_flight(json_t *flight, json_t *planet, json_t *settings, int n);
