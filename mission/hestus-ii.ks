FUNCTION input {
	PARAMETER t.
	PARAMETER n.
	PARAMETER c.
	LOCAL a IS 1.0.
	LOCAL b IS 1-t.
	LOCAL u IS a*c[0][0] + b*c[1][0].
	LOCAL w IS a*c[0][1] + b*c[1][1].
	FROM {LOCAL i IS 2.} UNTIL i = n STEP {SET i TO i+1.} DO {
		LOCAL x IS b.
		SET b TO ((2*i+1-t)*b - i*a) / (i+1).
		SET a TO x.
		SET u TO u + c[i][0]*b.
		SET w TO w + c[i][1]*b.
	}
	RETURN LIST(u,w).
}SET coef TO LIST(LIST(0.1,0), LIST(0,0), LIST(0,0), LIST(0,0), LIST(0,0), LIST(0,0), LIST(0,0), LIST(0,0), LIST(0,0), LIST(0,0)).
