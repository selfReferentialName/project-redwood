#include "mesh.h"
#include <math.h>
#include <stdio.h>
#include <string.h>

static int latlines, lonlines;
static float radius;

static void gen(void)
{
	mesh.length = latlines*lonlines + lonlines + 2;
	mesh.pos = malloc(mesh.length*3*sizeof(float));
	mesh.nrm = malloc(mesh.length*3*sizeof(float));
	mesh.uv = malloc(mesh.length*2*sizeof(float));
	mesh.idlen = latlines*lonlines*2*3;
	mesh.id = malloc(mesh.idlen*sizeof(int));

	for (int i=0; i<latlines; i++) {
		for (int j=0; j<lonlines; j++) {
			size_t idx = i*lonlines + j;
			float theta = i*2*M_PI/latlines - M_PI;
			float h = 1.0 / (lonlines+2);
			float phi = (1-2*h)*j*M_PI/(lonlines-1) + h*M_PI - M_PI_2;

			mesh.pos[idx][0] = cos(theta)*cos(phi)*radius;
			mesh.pos[idx][1] = sin(phi)*radius;
			mesh.pos[idx][2] = sin(theta)*cos(phi)*radius;

			printf("(%d,%d) -> (%g,%g) -> (%g,%g,%g)\n", i, j, theta, phi,
					mesh.pos[idx][0], mesh.pos[idx][1], mesh.pos[idx][2]);

			memcpy(mesh.nrm[idx], mesh.pos[idx], 3*sizeof(float));
			normalise(mesh.nrm[idx]);

			mesh.uv[idx][0] = 1 - (theta/M_PI + 1) / 2;
			mesh.uv[idx][1] = -phi/M_PI + 0.5;

			mesh.id[idx*6+0*3+0] = (j<lonlines-1) ? idx+1 : mesh.length-2;
			mesh.id[idx*6+0*3+1] = idx + lonlines;
			if (i >= latlines-1) mesh.id[idx*6+0*3+1] = latlines*lonlines + j;
			mesh.id[idx*6+0*3+2] = idx;

			if (i != 0) {
				mesh.id[idx*6+1*3+0] = (j>0) ? idx-1 : mesh.length-1;
				mesh.id[idx*6+1*3+1] = idx - lonlines;
				mesh.id[idx*6+1*3+2] = idx;
			} else {
				mesh.id[idx*6+1*3+0] = (j>0) ? latlines*lonlines + j - 1 : mesh.length-1;
				mesh.id[idx*6+1*3+1] = (latlines-1)*lonlines + j;
				mesh.id[idx*6+1*3+2] = latlines*lonlines + j;
			}
		}
	}

	mesh.pos[mesh.length-2][0] = mesh.nrm[mesh.length-2][0] = 0;
	mesh.pos[mesh.length-2][1] = radius;
	mesh.pos[mesh.length-2][2] = mesh.nrm[mesh.length-2][2] = 0;
	mesh.nrm[mesh.length-2][1] = 1;
	mesh.uv[mesh.length-2][0] = 0.5;
	mesh.uv[mesh.length-2][1] = 0;

	mesh.pos[mesh.length-1][0] = mesh.nrm[mesh.length-1][0] = 0;
	mesh.pos[mesh.length-1][1] = -radius;
	mesh.pos[mesh.length-1][2] = mesh.nrm[mesh.length-1][2] = 0;
	mesh.nrm[mesh.length-1][1] = -1;
	mesh.uv[mesh.length-1][0] = 0.5;
	mesh.uv[mesh.length-1][1] = 1;

	for (int i=0; i<lonlines; i++) {
		size_t idx = latlines*lonlines + i;
		float h = 1.0 / (lonlines+2);
		float phi = (1-2*h)*i*M_PI/(lonlines-1) + h*M_PI - M_PI_2;

		mesh.pos[idx][0] = -cos(phi)*radius;
		mesh.pos[idx][1] = sin(phi)*radius;
		mesh.pos[idx][2] = 0;

		memcpy(mesh.nrm[idx], mesh.pos[idx], 3*sizeof(float));
		normalise(mesh.nrm[idx]);

		mesh.uv[idx][0] = 0;
		mesh.uv[idx][1] = -phi/M_PI + 0.5;
	}
}

int main(int argc, char **argv)
{
	latlines = atoi(argv[1]);
	lonlines = atoi(argv[2]);
	outbase = argv[3];
	radius = atof(argv[4]);

	printf("Generating sphere at %s with %dx%d vertices and radius %g.\n", outbase, latlines, lonlines, radius);

	gen();
	writemesh();
}
