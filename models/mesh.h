#include <stdlib.h>

extern char *outbase;

extern struct mesh {
	size_t length;
	float (*pos)[3];
	float (*nrm)[3];
	float (*uv)[2];
	size_t idlen;
	int *id;
} mesh;

void normalise(float *vec);
void orthogonalise(const float *wrt, float *vec);
void writemesh(void);
