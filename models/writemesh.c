#include "mesh.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

char *outbase;

struct mesh mesh;

void normalise(float *vec)
{
	float norm = sqrt(vec[0]*vec[0] + vec[1]*vec[1] + vec[2]*vec[2]);
	vec[0] /= norm;
	vec[1] /= norm;
	vec[2] /= norm;
}

void orthogonalise(const float *wrt, float *vec)
{
	float dot = wrt[0]*vec[0] + wrt[1]*vec[1] + wrt[2]*vec[2];
	vec[0] -= dot*wrt[0];
	vec[1] -= dot*wrt[1];
	vec[2] -= dot*wrt[2];
	normalise(vec);
}

void writemesh(void)
{
	char *posname, *nrmname, *uvname, *idname;
	FILE *posfile, *nrmfile, *uvfile, *idfile;

	posname = malloc(strlen(outbase)+16);
	sprintf(posname, "%s-position.bin", outbase);
	posfile = fopen(posname, "w");
	fwrite(mesh.pos, sizeof(float)*3, mesh.length, posfile);
	fclose(posfile);
	free(posname);

	nrmname = malloc(strlen(outbase)+16);
	sprintf(nrmname, "%s-normal.bin", outbase);
	nrmfile = fopen(nrmname, "w");
	fwrite(mesh.nrm, sizeof(float)*3, mesh.length, nrmfile);
	fclose(nrmfile);
	free(nrmname);

	uvname = malloc(strlen(outbase)+16);
	sprintf(uvname, "%s-uv.bin", outbase);
	uvfile = fopen(uvname, "w");
	fwrite(mesh.uv, sizeof(float)*2, mesh.length, uvfile);
	fclose(uvfile);
	free(uvname);

	if (mesh.id) {
		idname = malloc(strlen(outbase)+16);
		sprintf(idname, "%s-id.bin", outbase);
		idfile = fopen(idname, "w");
		fwrite(mesh.id, sizeof(int)*2, mesh.idlen, idfile);
		fclose(idfile);
		free(idname);
	}
}
