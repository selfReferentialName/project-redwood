#include "log.h"
#include "physics.h"
#include <string.h>

int state_section_len[NUM_STATE_SECTIONS] = {
	[STS_INERTIAL] = 6,
	[STS_MASS] = 1,
};

static int seclen(int sec)
{
	if (sec < 0) {
		return -sec;
	} else {
		log_assert(sec < NUM_STATE_SECTIONS);
		return state_section_len[sec];
	}
}

static void run_law(int n, dfloat t, dfloat state[n], dfloat deriv[n], struct phys_law_collection *laws, struct phys_law *self)
{
	if (!self->_extra) {
		int extra_size = 0;
		for (int i=0; i<self->extrac; i++) {
			extra_size += state_section_len[self->extrav[i]];
		}
		if (extra_size) {
			self->_extra = malloc(extra_size * sizeof(dfloat));
		} else {
			self->_extra = (void *)1;
		}
	}
	dfloat *astate, *aderiv;
	int extraidx = 0, stateidx = 0;
	int affectlen = -1;
	for (int i=0; i<laws->secc; i++) {
		log_assert(stateidx < n);
		int sclen = seclen(laws->secv[i]);
		if (laws->secv[i] == self->affected) {
			astate = state + stateidx;
			aderiv = deriv + stateidx;
			affectlen = sclen;
		} else {
			for (int j=0; j<self->extrac; j++) {
				if (laws->secv[i] == self->extrav[j]) {
					memcpy(self->_extra+extraidx, state+stateidx, sclen*sizeof(dfloat));
					extraidx += sclen;
					break;
				}
			}
		}
		stateidx += sclen;
	}
	log_assert(affectlen >= 0);
	memset(laws->_deriv, 0, sizeof(dfloat) * affectlen);
	self->f(t, astate, self->_extra, laws->_deriv, self->data);
	for (int i=0; i<affectlen; i++) {
		aderiv[i] += laws->_deriv[i];
	}
}

void run_phys_laws(int n, dfloat t, dfloat state[n], dfloat deriv[n], void *data)
{
	struct phys_law_collection *self = data;
	memset(deriv, 0, sizeof(dfloat)*n);
	if (!self->_deriv) self->_deriv = malloc(n * sizeof(dfloat));
	for (int i=0; i<self->lawc; i++) {
		run_law(n, t, state, deriv, self, self->lawv+i);
	}
}

static int section_compare(const void *pa, const void *pb)
{
	int a = *(int*)pa;
	int b = *(int*)pb;
	if (a >= 0 && b >= 0) {
		return b - a;
	} else if (a >= 0) {
		return -1;
	} else if (b >= 0) {
		return 1;
	} else {
		return a - b;
	}
}

// this sorting algorithm is O(n^3)
static void insert_section(struct phys_law_collection *self, int section, int *secb, int *length)
{
	if (self->secc == 0) {
		self->secv[0] = section;
		*length = seclen(section);
		self->secc = 1;
	}
	for (int i=0; i<self->secc; i++) {
		int cv = section_compare(&self->secv[i], &section);
		if (cv == 0) {
			return;
		} else if (cv < 0) {
			self->secc++;
			if (self->secc >= *secb) {
				*secb *= 2;
				log_assert(self->secv = realloc(self->secv, *secb * sizeof(struct phys_law)));
			}
			// move everything up one and put undefined data in self->secv[i]
			self->secv[self->secc-1] = 0xDEADBEEF;
			for (int j=self->secc-1; j>=i+1; j--) {
				int tmp = self->secv[j];
				self->secv[j] = self->secv[j-1];
				self->secv[j-1] = tmp;
			}
			self->secv[i] = section;
			*length += seclen(section);
			return;
		} else if (i == self->secc-1) {
			self->secc++;
			if (self->secc >= *secb) {
				*secb *= 2;
				log_assert(self->secv = realloc(self->secv, *secb * sizeof(struct phys_law)));
			}
			self->secv[self->secc-1] = section;
			*length += seclen(section);
		}
	}
}

void phys_laws_setup(struct phys_law_collection *self, struct ode *ode)
{
	int secb = 4;
	log_assert(self->secv = malloc(secb * sizeof(struct phys_law)));
	self->secc = 0;
	logfmt(LOG_VERBOSE, "setting up %d laws", self->lawc);
	for (int i=0; i<self->lawc; i++) {
		struct phys_law *law = self->lawv + i;
		insert_section(self, law->affected, &secb, &ode->n);
		for (int j=0; j<law->extrac; j++) {
			insert_section(self, law->extrav[j], &secb, &ode->n);
		}
	}

	ode->f = run_phys_laws;
	ode->data = self;
}
