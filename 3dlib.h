#include <math.h>

#pragma once

// these are all implementations of basic vector space operations
// the return value is in the final argument

inline void scale_3d(double alpha, double x[3], double y[3])
{
	for (int i=0; i<3; i++) {
		y[i] = alpha * x[i];
	}
}

inline void axpy_3d(double alpha, double x[3], double y[3], double z[3])
{
	for (int i=0; i<3; i++) {
		z[i] = fma(x[i], alpha, y[i]);
	}
}

// these are all implementations of the standard math.h functions but working on double[3]s
// the input can freely overlap with output except where restrict keywords are used
// the return value is put in the final argutment

// these functions are not based on math.h

inline double dot_3d(double x[3], double y[3])
{
	return x[0]*y[0] + x[1]*y[1] + x[2]*y[2];
}

inline double norm2_3d(double x[3])
{
	return sqrt(x[0]*x[0] + x[1]*x[1] + x[2]*x[2]);
}

inline void setlen2_3d(double alpha, double x[3], double y[3])
{
	scale_3d(alpha/norm2_3d(x), x, y);
}

inline double veccos_3d(double x[3], double y[3])
{
	return dot_3d(x,y) / norm2_3d(x) / norm2_3d(y);
}

inline double vecang_3d(double x[3], double y[3])
{
	return acos(veccos_3d(x, y));
}
