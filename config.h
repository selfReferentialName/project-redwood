#pragma once

#include <float.h>

// default floating point type, and info about it
#define dfloat double
#define EPS_DF DBL_EPSILON
#define DIG_DF DBL_DIG
