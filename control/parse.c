#include "control.h"
#include "log.h"
#include <jansson.h>
#include <math.h>
#include <string.h>

static dfloat input_deriv_thikonov(int m, dfloat *dudt, void *data)
{
	dfloat *weight = data;
	dfloat r = 0;
	for (int i=0; i<m; i++) {
//		logfmt(LOG_VERBOSE, "r += (%g*%g)^2", dudt[i], weight[i]);
		dfloat du2 = dudt[i]*weight[i];
		r += du2*du2;
	}
//	logfmt(LOG_VERBOSE, "r2 = %g", r);
	return sqrt(r);
}

void select_goal(json_t *in, struct optctrl_options *out, struct stage_data *stage, struct atmosphere_data *atmo)
{
	const char *goal;
	log_assert(goal = json_string_value(json_object_get(in, "goal")));
	if (strcmp(goal, "downrange_distance") == 0) {
		out->goal = downrange_distance;
	} else {
		log_unreachable();
	}
}

void select_input_cost(json_t *in, struct optctrl_options *out, struct stage_data *stage, struct atmosphere_data *atmo)
{
	const char *penalty;
	penalty = json_string_value(json_object_get(in, "input_penalty"));
	if (penalty == NULL) {
	} else if (strcmp(penalty, "velocity_alignment") == 0) {
		out->input_cost = velocity_alignment_cost;
		log_assert(out->data_input_cost = malloc(sizeof(dfloat)));
		log_assert(*(dfloat*)out->data_input_cost = json_real_value(json_object_get(in, "misalign_cost")));
	} else {
		log_unreachable();
	}
	penalty = json_string_value(json_object_get(in, "input_deriv_penalty"));
	if (penalty == NULL) {
	} else if (strcmp(penalty, "thikonov") == 0) {
		out->input_deriv_cost = input_deriv_thikonov;
		int m = out->system.m;
		log_assert(out->data_input_deriv_cost = malloc(m*sizeof(dfloat)));
		dfloat *weights = out->data_input_deriv_cost;
		json_t *weight;
		if (weight = json_object_get(in, "input_deriv_cost_array")) {
			for (int i=0; i<m; i++) {
				log_assert(weights[i] = json_real_value(json_array_get(weight, i)));
			}
		} else if (weight = json_object_get(in, "input_deriv_cost")) {
			dfloat w;
			log_assert(w = json_real_value(weight));
			for (int i=0; i<m; i++) {
				weights[i] = w;
			}
		} else {
			log_unreachable();
		}
	} else {
		log_unreachable();
	}
	out->state_cost = stay_above_ground;
}

void parse_control(json_t *in, struct optctrl_options *out, struct stage_data *stage, struct atmosphere_data *atmo)
{
	int n = out->system.ode.n;
	int m = out->system.m = 2;
	select_goal(in, out, stage, atmo);
	select_input_cost(in, out, stage, atmo);
	json_t *bounds = json_object_get(in, "input_bounds");
	if (bounds) {
		log_assert(out->input_min = malloc(m * sizeof(dfloat)));
		log_assert(out->input_max = malloc(m * sizeof(dfloat)));
		for (int i=0; i<m; i++) {
			json_t *me;
			log_assert(me = json_array_get(bounds, i));
			log_assert(out->input_min[i] = json_real_value(json_array_get(me, 0)));
			log_assert(out->input_max[i] = json_real_value(json_array_get(me, 1)));
		}
	}
	log_assert((bool)(out->ctol = json_real_value(json_object_get(in, "cost_tol"))) |
			(bool)(out->xtol = json_real_value(json_object_get(in, "input_tol"))));
	log_assert(out->maxtime = json_real_value(json_object_get(in, "max_time")));
	log_assert(out->time_horizon = json_real_value(json_object_get(in, "time_horizon")));
	// TODO: make options
	out->alg = OCA_C_COBYLA;
	out->colocation.basis = laguerre;
	log_assert(out->colocation.k = json_integer_value(json_object_get(in, "collocation_basis_size")));
	out->system.in = directed_thrust;
	out->system.m = 2;
	out->system.data = stage;
}
