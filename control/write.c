#include "control.h"
#include "log.h"

static const char *chebyshev_prelude = "\
FUNCTION input {\n\
	PARAMETER t.\n\
	PARAMETER n.\n\
	PARAMETER c.\n\
	LOCAL a IS 1.0.\n\
	LOCAL b IS t.\n\
	LOCAL u IS a*c[0][0] + b*c[0][1].\n\
	LOCAL w IS a*c[1][0] + b*c[1][1].\n\
	FROM {LOCAL i IS 2.} UNTIL i = n STEP {SET i TO i+1.} DO {\n\
		LOCAL x IS b.\n\
		SET b TO 2*t*b - a.\n\
		SET a TO x.\n\
		SET u TO u + c[0][i]*b.\n\
		SET w TO w + c[1][i]*b.\n\
	}\n\
	RETURN LIST(u,w).\n\
}";

static const char *laguerre_prelude = "\
FUNCTION input {\n\
	PARAMETER t.\n\
	PARAMETER n.\n\
	PARAMETER c.\n\
	LOCAL a IS 1.0.\n\
	LOCAL b IS 1-t.\n\
	LOCAL u IS a*c[0][0] + b*c[1][0].\n\
	LOCAL w IS a*c[0][1] + b*c[1][1].\n\
	FROM {LOCAL i IS 2.} UNTIL i = n STEP {SET i TO i+1.} DO {\n\
		LOCAL x IS b.\n\
		SET b TO ((2*i+1-t)*b - i*a) / (i+1).\n\
		SET a TO x.\n\
		SET u TO u + c[i][0]*b.\n\
		SET w TO w + c[i][1]*b.\n\
	}\n\
	RETURN LIST(u,w).\n\
}";

void write_control(struct openloop *in, FILE *out)
{
	logfmt(LOG_DEFAULT, "Writing coefficients for basis of %d functions", in->_collocation.k);
	if (in->_collocation.basis == chebyshev) {
		fputs(chebyshev_prelude, out);
	} else if (in->_collocation.basis == laguerre) {
		fputs(laguerre_prelude, out);
	} else {
		log_unreachable();
	}
	fputs("SET coef TO LIST(", out);
	for (int i=0; i<in->_collocation.k; i++) {
		if (i>0) fputs(", ", out);
		dfloat *arr = in->_collocation.coldat;
		logfmt_assert(arr, "coefficients of %dth basis element NULL", i);
		fprintf(out, "LIST(%g,%g)", arr[i], arr[i+in->_collocation.k]);
		logfmt(LOG_DEFAULT, "Coefficients for basis function %d are (%g,%g)", i, arr[i], arr[i+in->_collocation.k]);
	}
	fputs(").\n", out);
}
