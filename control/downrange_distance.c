#include "control.h"
#include "log.h"
#include "3dlib.h"
#include <math.h>

dfloat downrange_distance(int n, dfloat *state, void *data)
{
//	logfmt(LOG_VERBOSE, "%g,%g,%g\t%g,%g,%g\t%g", state[0], state[1], state[2], state[3], state[4], state[5], state[6]);
	double rho = norm2_3d(state);
	double spc[3] = {1, 0, 0};
	double radius = 6371e3;
	if (rho < radius) {
		return M_PI*radius - vecang_3d(spc, state)*radius + (radius-rho)*radius;
	} else {
		return M_PI*radius - vecang_3d(spc, state)*radius;
	}
}

dfloat stay_above_ground(int n, dfloat *state, void *data)
{
	double rho = norm2_3d(state);
	double radius = 6371e3;
	double everest = 8848+radius;
	if (rho < radius) {
		return (radius - rho) * 1000 + 100*everest;
	} else if (rho < everest) {
		return (everest - rho) * 100;
	} else {
		return 0;
	}
}
