#include "control.h"
#include "log.h"
#include "3dlib.h"
#include <math.h>

static void get_dir(dfloat *input, double dir[3])
{
	dfloat theta = input[0]; // azimuth
	dfloat phi = input[1]; // inclination
	dir[0] = sin(theta)*cos(phi);
	dir[1] = cos(theta)*cos(phi);
	dir[2] = sin(phi);
}

void directed_thrust(int n, dfloat *state, int m, dfloat *input, int n2, dfloat *deriv, void *data)
{
	struct stage_data *self = data;
	log_assert(n == n2);
	log_assert(m == 2);

	double dir[3];
	get_dir(input, dir);

//	logfmt(LOG_VERBOSE, "%g,%g %g,%g,%g, %g,%g", theta,phi, dir[0],dir[1],dir[2], self->thrust,state[6]);
	dfloat mass = state[6] > 1e-5 ? state[6] : 1e-5;
	scale_3d(self->thrust/state[6], dir, deriv+3);
//	logfmt(LOG_VERBOSE, "%g,%g,%g", deriv[3], deriv[4], deriv[5]);
}

dfloat velocity_alignment_cost(int m, dfloat *input, int n, dfloat *state, void *data)
{
	dfloat penalty = *(dfloat*)data;
	double dir[3];
	get_dir(input, dir);
	double miscos = veccos_3d(dir, state+3);
//	logfmt(LOG_VERBOSE, "miscos = %g", miscos);
	miscos = fabs(miscos);
	dfloat c0 = penalty / (miscos+1);
//	logfmt(LOG_VERBOSE, "c0 = %g, penalty = %g, miscos = %g", c0, penalty, miscos);
	return c0;
}
