#pragma once

#include "physics.h"
#include <jansson.h>
#include <stdio.h>

// x \in R^n, y \in R^m, z \in R^p
// z = f(x,y)
typedef void vecvecfun(int n, dfloat *x, int m, dfloat *y, int p, dfloat *z, void *data);

// u \in R^{n}
// evaluate basis at time t
// if dudt is not NULL, it gets the time derivative
typedef void ctrlbasis(int n, dfloat t, dfloat *u, dfloat *dudt);

ctrlbasis chebyshev;
ctrlbasis laguerre;

struct control_system {
	struct ode ode;
	int m, p; // number of inputs and outputs, respectively
	vecvecfun *in, *out; // input and output functions
	void *data; // passed to in and out
};

enum optctrl_alg {
	// colocation methods
	// see https://nlopt.readthedocs.io/en/latest/NLopt_Algorithms/
	OCA_C_COBYLA,
	OCA_C_DIRECT,
};

struct optctrl_options {
	// public data
	struct control_system system;
	dfloat (*input_cost)(int m, dfloat *input, int n, dfloat *state, void *data); // a penalty on changing the inputs
	fnl_fd *state_cost; // a penalty on the system state
	fnl_fd *input_deriv_cost; // a penalty on the time derivative of the input
	void *data_input_cost; // passed to input_cost
	void *data_state_cost; // passed to state_cost
	void *data_input_deriv_cost; // passed to input_deriv_cost
	fnl_fd *goal; // a penalty on the final state of the system; input considered done when zero
	void *data_goal; // passed to goal
	dfloat *input_min, *input_max; // AABB bounds on the input and output values
	dfloat time_horizon; // the time to make inputs up to (without effects of goal)
	dfloat ctol, xtol; // absolute tolerance on change in cost (input_cost+output_cost+goal) and x, respectively
	dfloat maxtime; // maximum time to spend optimising
	enum optctrl_alg alg; // algorithm to use
	union {
		struct {
			ctrlbasis *basis;
			int k;
		} colocation;
	};

	// private stuff
	struct ode _input_adjoined;
};

void parse_control(json_t *in, struct optctrl_options *out, struct stage_data *stage, struct atmosphere_data *atmo);

struct openloop {
	// public data
	struct ode *base;
	vecvecfun *input;
	void *inputdata;
	int m;

	// private data
	union {
		struct {
			ctrlbasis *basis;
			dfloat *coldat;
			int k;
		} _collocation;
	};
	dfloat *_atmp; // input changes for collocation
	dfloat *_btmp; // basis output for collocation
};

// get an ode that models the system with the open loop input
void openloopsim(struct openloop *self, struct ode *out);

void write_control(struct openloop *in, FILE *out);

// calculate the optimal control for something
void calc_optctrl(struct optctrl_options *opts, struct openloop *out);

////////// Input Functions \\\\\\\\\\

// takes in struct stage_data as data
// inputs are spherical coordinate angles (azimuth, inclination) in radians
// we recommend bounding below by {-pi, -pi/2} and above by {pi, pi/2}
// any bounds of the same lengths should work too
vecvecfun directed_thrust;

////////// Goal Funcitons \\\\\\\\\\

// takes in struct atmosphere_data as data
fnl_fd downrange_distance;

////////// Cost Functions \\\\\\\\\\

// penalise not pointing at the velocity vector
// gives infinite cost for pointing directly away
// data is a scaling dfloat
dfloat velocity_alignment_cost(int m, dfloat *input, int n, dfloat *cost, void *data);

// penalise masively for being below sea level and mildly for being below the peak of mount everest
// this is done in a continuous and piecewise smooth way
fnl_fd stay_above_ground;
