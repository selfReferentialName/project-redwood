#include "log.h"
#include "physics.h"
#include "3dlib.h"
#include <math.h>
#include <string.h>

static float state_to_altitude(dfloat *inertial, struct atmosphere_data *self)
{
	dfloat rho = norm2_3d(inertial+0);
	return rho - self->radius;
}

static float state_to_geopot_alt(dfloat *inertial, struct atmosphere_data *self)
{
	dfloat rho = norm2_3d(inertial+0);
	return self->base.mu/3/self->radius/self->radius - self->base.mu/3/rho/rho;
}

static float state_to_pressure(dfloat *inertial, struct atmosphere_data *self)
{
	float hb;
	float h = state_to_geopot_alt(inertial, self);
	int layer = 0;
	for (int i=0; i<self->layers; i++) {
		if (h >= self->layer_alt[i]) {
			layer = i;
			hb = self->layer_alt[i];
		}
	}
	float Pb = self->base_prs[layer];
	float Tb = self->base_tmp[layer];
	float Lb = -self->delta_tmp[layer];
	float g = self->base.mu / norm2_3d(inertial+0) / norm2_3d(inertial+0);

	if (Lb == 0) {
		return Pb * expf(-g / self->specgas * (h-hb) / Tb);
	} else {
		return Pb * pow((Tb-(h-hb)*Lb)/Tb, g/self->specgas/Lb - 1);
	}
}

static float state_to_temperature(dfloat *inertial, struct atmosphere_data *self)
{
	float hb;
	float h = state_to_geopot_alt(inertial, self);
	int layer = 0;
	if (h >= self->layer_alt[0]) {
		for (int i=0; i<self->layers; i++) {
			if (h >= self->layer_alt[i]) {
				layer = i;
				hb = self->layer_alt[i];
			}
		}
	} else {
		layer = 0;
		hb = self->layer_alt[0];
	}
	float Tb = self->base_tmp[layer];
	float dT = self->delta_tmp[layer];
	return Tb + (h-hb)*dT;
}

static float state_to_density(dfloat *inertial, struct atmosphere_data *self)
{
	return state_to_pressure(inertial, self) / self->specgas / state_to_temperature(inertial, self);
}

static float state_to_mach(dfloat *inertial, struct atmosphere_data *self)
{
	float speed = norm2_3d(inertial+3);
	float c = sqrtf(self->adiabic * self->specgas * state_to_temperature(inertial, self));
	return speed / c;
}

// TODO: assumes constant coefficient of drag when supersonic (it should go down)
static void state_drag(dfloat *inertial, struct atmosphere_data *atm, struct stage_data *stage, double force[3])
{
	float M = state_to_mach(inertial, atm);
	float dord = state_to_density(inertial, atm);
	dfloat cd;
	if (M < stage->Mcr) {
		cd = stage->cd0;
	} else if (M < 1) {
		cd = (M-1)*stage->cd0 + M*stage->cd1;
	} else {
		cd = stage->cd1;
	}

	scale_3d(-dord*norm2_3d(inertial+3)*cd*stage->area, inertial+3, force);
}

static void run(dfloat t, dfloat *inertial, dfloat *extra, dfloat *deriv, void *data)
{
//	logfmt(LOG_VERBOSE, "%g,%g,%g,%g,%g,%g,%g\t%g", t, inertial[0],inertial[1],inertial[2],inertial[3],inertial[4],inertial[5], extra[0]);
	log_assert(data);
	dfloat mass = extra[0]>1e-5 ? extra[0] : 1e-5;
	struct atmosim_data *self = data;
	state_drag(inertial, &self->atm, &self->stage, deriv+3);
	for (int i=3; i<6; i++) {
		deriv[i] /= mass;
	}
}

int sts_mass = STS_MASS;

const struct phys_law atmosim = {
	.f = run,
	.affected = STS_INERTIAL,
	.extrac = 1,
	.extrav = &sts_mass
};
