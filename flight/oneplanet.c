#include "log.h"
#include "physics.h"
#include "3dlib.h"

static void run(dfloat t, dfloat *inertial, dfloat *unused, dfloat *deriv, void *data)
{
	log_assert(data);
	struct planet_data *self = data;
	dfloat rho = norm2_3d(inertial+0);
	setlen2_3d(-self->mu/rho/rho, inertial+0, deriv+3);
}

const struct phys_law oneplanet = {
	.f = &run,
	.affected = STS_INERTIAL,
	.extrac = 0
};
