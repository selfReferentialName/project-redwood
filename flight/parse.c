#include "physics.h"
#include "log.h"
#include <jansson.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

static void add_needed_component(int **arr, int *count, int component)
{
	for (int i=0; i<*count; i++) {
		if (arr[0][i] == component) return;
	}
	(*count)++;
	*arr = realloc(*arr, *count * sizeof(int));
	arr[0][*count-1] = component;
}

static void add_disc(struct ode *self, int idx)
{
	add_needed_component(&self->state_discs, &self->state_disc_count, idx);
}

static dfloat dry_test(int n, dfloat state[n], void *data)
{
	dfloat drymass = *(dfloat*)data;
	if (drymass > state[6]*1.01) {
		return 0;
	} else {
		return 1;
	}
}

static struct phys_law_collection *copy_law0_atmosim(void *data)
{
	struct phys_law_collection *orig = data;
	struct phys_law_collection *self = malloc(sizeof(struct phys_law_collection));
	log_assert(self);
	log_assert(data);
	*self = *orig;
	self->lawv = malloc(sizeof(struct phys_law) * self->lawc);
	memcpy(self->lawv, orig->lawv, sizeof(struct phys_law)*self->lawc);
	self->lawv[0].data = malloc(sizeof(struct atmosim_data));
	memcpy(self->lawv[0].data, orig->lawv[0].data, sizeof(struct atmosim_data));
	return self;
}

static void parse_stage(json_t *in, struct ode *arr, int idx, int n, dfloat payload, struct ode *template)
{
	log_assert(in);
	struct ode *self = arr+idx;
	*self = *template;
	log_assert(self->state = malloc(n*sizeof(dfloat)));

	json_t *r0 = json_object_get(in, "initial_position");
	if (r0) {
		log_assert(idx == 0);
		log_assert(json_array_size(r0) == 3);
		for (int i=0; i<3; i++) {
			self->state[i] = json_real_value(json_array_get(r0, i));
		}
	} else if (idx == 0) {
		logfmt(LOG_ERROR, "Stage %d should have an initial_position", idx);
		logstr(json_dumps(in, 0), LOG_ERROR); // leaks memory for like 5 microseconds
		log_unreachable();
	}
	json_t *v0 = json_object_get(in, "initial_velocity");
	if (v0) {
		log_assert(idx == 0);
		log_assert(json_array_size(v0) == 3);
		for (int i=0; i<3; i++) {
			self->state[i+3] = json_real_value(json_array_get(v0, i));
		}
	} else if (idx == 0) {
		memset(self->state+3, 0, 3*sizeof(dfloat));
	}

	json_t *m0 = json_object_get(in, "wet_mass");
	if (m0) {
		log_assert(self->state[6] = json_real_value(m0));
		self->state[6] += payload;
		add_disc(self, 6);
	}
	json_t *m1 = json_object_get(in, "dry_mass");
	json_t *bt = json_object_get(in, "burn_time");
	json_t *rt = json_object_get(in, "run_time");
	if (m1) {
		log_assert(self->fnext_data = malloc(sizeof(dfloat)));
		log_assert(*(dfloat*)self->fnext_data = json_real_value(m1));
		self->fnext = &dry_test;
		int next_idx;
		log_assert(next_idx = json_integer_value(json_object_get(in, "on_dry")));
		if (next_idx > 0) {
			self->next_f = arr + next_idx;
		} else {
			self->next_f = &ode_done;
		}
	} else if (bt) {
		log_assert(self->tnext = json_real_value(bt));
		int next_idx;
		log_assert(next_idx = json_integer_value(json_object_get(in, "on_dry")));
		if (next_idx > 0) {
			self->next_f = arr + next_idx;
		} else {
			self->next_f = &ode_done;
		}
	} else if (rt) {
		log_assert(self->tnext = json_real_value(rt));
		int next_idx;
		log_assert(next_idx = json_integer_value(json_object_get(in, "on_timeout")));
		if (next_idx > 0) {
			self->next_f = arr + next_idx;
		} else {
			self->next_f = &ode_done;
		}
	}

	json_t *burntimej = json_object_get(in, "burn_time");
	json_t *drytwrj = json_object_get(in, "dry_twr");
	json_t *wettwrj = json_object_get(in, "wet_twr");
	json_t *thrustj = json_object_get(in, "thrust");
	json_t *ispj = json_object_get(in, "isp");
	dfloat thrust, isp;
	// get thrust
	if (thrustj) {
		thrust = json_real_value(thrustj);
	} else if (m1 && drytwrj) {
		thrust = json_real_value(drytwrj) * json_real_value(m1) * 9.81;
	} else if (m0 && wettwrj) {
		thrust = json_real_value(wettwrj) * json_real_value(m0) * 9.81;
	} else {
		log_unreachable();
	}
	// get specific impulse
	if (ispj) {
		isp = json_real_value(ispj);
	} else if (m0 && m1 && burntimej) {
		dfloat M1 = json_real_value(m1);
		dfloat M0 = json_real_value(m0);
		isp = thrust / 9.81 / (M1-M0)*json_real_value(burntimej);
	} else if (thrust == 0) {
	} else {
		log_unreachable();
	}
	// write results
	struct phys_law_collection *laws = copy_law0_atmosim(self->data);
	self->data = laws;
	struct atmosim_data *atmosim_data = laws->lawv[0].data;
	atmosim_data->stage.thrust = thrust;
	atmosim_data->stage.isp = isp;
	laws->lawv[3].data = &atmosim_data->stage;

	log_assert(self->initstate = malloc(n*sizeof(dfloat)));
	memcpy(self->initstate, self->state, n*sizeof(dfloat));

	// defaults taken from Saturn V rocket
	atmosim_data->stage.cd0 = json_real_value(json_object_get(in, "cd_still"));
	if (!atmosim_data->stage.cd0) atmosim_data->stage.cd0 = 0.3;
	atmosim_data->stage.cd1 = json_real_value(json_object_get(in, "cd_mach_1"));
	if (!atmosim_data->stage.cd1) atmosim_data->stage.cd1 = atmosim_data->stage.cd0 * 2;
	atmosim_data->stage.Mcr = json_real_value(json_object_get(in, "critical_mach"));
	if (!atmosim_data->stage.Mcr) atmosim_data->stage.Mcr = 0.6;
	log_assert(atmosim_data->stage.area = json_real_value(json_object_get(in, "drag_area")));
	logfmt(LOG_VERBOSE, "stage %d: cd0=%g, cd1=%g, Mcr=%g", idx,
			atmosim_data->stage.cd0, atmosim_data->stage.cd1, atmosim_data->stage.Mcr);

	self->controlled = json_is_true(json_object_get(in, "controlled"));
	if (!self->controlled) {
		if (thrust) {
			laws->lawc++;
			laws->lawv = realloc(laws->lawv, laws->lawc*sizeof(struct phys_law));
			laws->lawv[laws->lawc-1] = posigrade_thrust;
			laws->lawv[laws->lawc-1].data = &atmosim_data->stage;
		}
	}
}

struct ode *setup_template(struct ode *ode, json_t *planet, json_t *settings)
{
	*ode = ode_skel;
	ode->rtol = json_real_value(json_object_get(settings, "reltol"));
	ode->atol = json_real_value(json_object_get(settings, "abstol"));

	log_assert(ode->data = calloc(1, sizeof(struct phys_law_collection)));
	struct phys_law_collection *laws = ode->data;
	laws->lawc = 4;
	log_assert(laws->lawv = malloc(laws->lawc * sizeof(struct phys_law)));

	laws->lawv[0] = atmosim;
	laws->lawv[0].data = malloc(sizeof(struct atmosim_data));
	logfmt(LOG_VERBOSE, "setup_template atmosim_data at %p", laws->lawv[0].data);
	struct atmosim_data *asd = laws->lawv[0].data;
	log_assert(asd->atm.base.mu = json_real_value(json_object_get(planet, "standard_gravitation")));
	log_assert(asd->atm.radius = json_real_value(json_object_get(planet, "radius")));
	log_assert(asd->atm.specgas = json_real_value(json_object_get(planet, "specific_gas")));
	if (!(asd->atm.adiabic = json_real_value(json_object_get(planet, "adiabatic_index"))) &&
			!(asd->atm.adiabic = json_real_value(json_object_get(planet, "heat_capacity_ratio")))) {
		log_unreachable();
	}
	logfmt(LOG_VERBOSE, "specific gas constant %g; adiabatic index %g", asd->atm.specgas, asd->atm.adiabic);
	if (asd->atm.layers = json_array_size(json_object_get(planet, "atmosphere_layers"))) {
		json_t *layers = json_object_get(planet, "atmosphere_layers");
		log_assert(layers);
		log_assert(asd->atm.layer_alt = malloc(asd->atm.layers * sizeof(float)));
		log_assert(asd->atm.base_tmp = malloc(asd->atm.layers * sizeof(float)));
		log_assert(asd->atm.delta_tmp = malloc(asd->atm.layers * sizeof(float)));
		log_assert(asd->atm.base_prs = malloc(asd->atm.layers * sizeof(float)));
		for (int i=0; i<asd->atm.layers; i++) {
			json_t *me = json_array_get(layers, i);
			log_assert(me);
			log_assert(asd->atm.layer_alt[i] = json_real_value(json_object_get(me, "base_altitude")));
			log_assert(asd->atm.base_tmp[i] = json_real_value(json_object_get(me, "base_temperature")));
			asd->atm.delta_tmp[i] = -json_real_value(json_object_get(me, "lapse_rate"))/1000;
			log_assert(asd->atm.base_prs[i] = json_real_value(json_object_get(me, "base_pressure")));
		}
	}
	asd->stage.Mcr = -666;

	laws->lawv[1] = oneplanet;
	laws->lawv[1].data = &asd->atm.base;
	laws->lawv[2] = inertial_fix;
	laws->lawv[3] = burn_mass_fix;

	phys_laws_setup(laws, ode);

	return ode;
}

struct ode *parse_flight(json_t *flight, json_t *planet, json_t *settings, int n)
{
	log_assert(flight);
	log_assert(planet);
	log_assert(settings);
	int stages = json_array_size(flight);
	log_assert(stages);
	struct ode *out = malloc(stages * sizeof(struct ode));
	log_assert(out);
	struct ode template = ode_skel;
	setup_template(&template, planet, settings);
	dfloat payload = json_real_value(json_object_get(settings, "payload_mass"));
	for (int i=0; i<stages; i++) {
		out[i] = template;
		parse_stage(json_array_get(flight, i), out, i, n, payload, out+i);
	}
	return out;
}
