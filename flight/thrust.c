#include "physics.h"
#include "3dlib.h"

static void posigrade(dfloat t, dfloat *inertial, dfloat *extra, dfloat *deriv, void *data)
{
	struct stage_data *self = data;
	dfloat mass = extra[0]>1e-5 ? extra[0] : 1e-5;
	setlen2_3d(self->thrust/mass, inertial+3, deriv+3);
}

static void burnmass(dfloat t, dfloat *mass, dfloat *unused, dfloat *deriv, void *data)
{
	struct stage_data *self = data;
	deriv[0] = self->thrust / 9.81 / self->isp;
}

static int sts_mass = STS_MASS;

const struct phys_law posigrade_thrust = {
	.f = posigrade,
	.affected = STS_INERTIAL,
	.extrac = 1,
	.extrav = &sts_mass
};

const struct phys_law burn_mass_fix = {
	.f = burnmass,
	.affected = STS_MASS
};
