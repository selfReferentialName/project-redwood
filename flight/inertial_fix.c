#include "physics.h"

static void run(dfloat t, dfloat *affected, dfloat *unused, dfloat *deriv, void *data)
{
	for (int i=0; i<3; i++) {
		deriv[i] = affected[i+3];
	}
}

const struct phys_law inertial_fix = {
	.f = run,
	.affected = STS_INERTIAL,
	.extrac = 0
};
