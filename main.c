#include "physics.h"
#include "control.h"
#include "log.h"
#include <jansson.h>
#include <stdio.h>

int main(int argc, char **argv)
{
	log_assert(argc == 3 || argc == 4);
	char *infile = argv[1];
	char *outfile = argv[2];

	json_error_t uhoh_json;
	json_t *input;
	log_assert(input = json_load_file(infile, 0, &uhoh_json));

	struct ode *ode = parse_flight(json_object_get(input, "rocket"),
			json_object_get(input, "planet"),
			json_object_get(input, "ode_settings"),
			7);
	struct atmosim_data *asd = ((struct phys_law_collection*)ode->data)->lawv[0].data;
	struct optctrl_options oco = { .system = { .ode = *ode } };
	parse_control(json_object_get(input, "control"), &oco, &asd->stage, &asd->atm);
	json_decref(input);

	struct openloop result;
	calc_optctrl(&oco, &result);
	FILE *f = fopen(outfile, "w");
	write_control(&result, f);
	fclose(f);

	if (argc >= 4) {
		char *simfile = argv[3];
		f = fopen(simfile, "w");
		struct ode controlled;
		openloopsim(&result, &controlled);
		int smpc = 128;
		dfloat *smpv = malloc(sizeof(dfloat)*7*smpc);
		controlled.eval(&controlled, oco.time_horizon,
				10, 0, 0, &smpc, &smpv,
				NULL, NULL, NULL);
		logfmt(LOG_VERBOSE, "%d", smpc);
		for (int i=0; i<smpc; i++) {
			fprintf(f, "%g,%g,%g,%g,%g,%g,%g,%g\n",
					smpv[i*8], // time
					smpv[i*8+1],smpv[i*8+2],smpv[i*8+3], // position
					smpv[i*8+4],smpv[i*8+5],smpv[i*8+6], // velocity
					smpv[i*8+7]); // mass
		}
	}

	return 0;
}
