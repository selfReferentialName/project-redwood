extends MeshInstance3D

var started = false
var data = Array()
var time = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	started = false
	var file = FileAccess.open("res://hestus-ii.csv", FileAccess.READ)
	print(file)
	var i = 0
	while not file.eof_reached():
		print(i)
		var line = file.get_csv_line()
		print(line)
		data.append(Array())
		for entry in line:
			data[i].append(entry.to_float())
		i = i + 1
	started = true

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if started:
		var idx = 0
		var t0 = 0
		for i in range(0,data.size()):
			if t0 < data[i][0]:
				idx = i
				t0 = data[i][0]
		position.x = data[idx][1]
		position.y = data[idx][2]
		position.z = data[idx][3]
		time = time + delta

func _on_file_dialog_confirmed():
#	var file = FileAccess.open($FileDialog.current_path, FileAccess.READ)
	var file = FileAccess.open("res:/hestus-ii.csv", FileAccess.READ)
	var i = 0
	while not file.eof_reached():
		var line = file.get_csv_line()
		data.append(Array())
		for entry in line:
			data[i].append(entry.to_float())
		i = i + 1
	started = true
