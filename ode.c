#include "log.h"
#include "physics.h"
#include <cvode/cvode.h>
#include <math.h>
#include <nvector/nvector_openmp.h>
#include <omp.h>
#include <string.h>
#include <sunlinsol/sunlinsol_dense.h>

#define cvassert(e, good) \
	do { \
		good = e; \
		if (good != CV_SUCCESS) { \
			logfmt(LOG_ALWAYS, "Cvode call " #e " failed with code %d\n", good); \
		} \
	} while (0)

static int cvrhs(dfloat t, N_Vector xv, N_Vector dxv, void *data)
{
	dfloat *x, *dx;
	struct ode *self = data;
	log_assert(x = N_VGetArrayPointer(xv));
	log_assert(dx = N_VGetArrayPointer(dxv));

	self->f(self->n, t, x, dx, self->data);
	return 0;
}

static int cvrfp(dfloat t, N_Vector x, dfloat *out, void *data)
{
	dfloat wnext_contrib=1, smp_r_contrib=1, smp_a_contrib=1, fnext_contrib=1;
	struct ode *self = data;
	if (self->next_w) {
		log_assert(!SUNMatMatvec(self->_wmnext, x, self->_tmpvecn));
		wnext_contrib = N_VDotProd(x, self->_tmpvecn);
	}
	if (self->_smp_r || self->_smp_a) {
		N_VLinearSum(1, x, -1, self->_smp_last_vec, self->_tmpvecn);
		dfloat delta = sqrt(N_VDotProd(self->_tmpvecn, self->_tmpvecn));
		if (self->_smp_r) {
			dfloat xnorm = sqrt(N_VDotProd(x, x));
			smp_r_contrib = fabs(delta/xnorm - self->_smp_r);
		}
		if (self->_smp_a) {
			smp_a_contrib = fabs(delta - self->_smp_a);
		}
	}
	if (self->fnext) {
		fnext_contrib = self->fnext(self->n, N_VGetArrayPointer(x), self->fnext_data);
	}
	out[0] = wnext_contrib * smp_r_contrib * smp_a_contrib * fnext_contrib;
	dfloat min_contrib = fmin(fnext_contrib, fmin(wnext_contrib, fmin(smp_r_contrib, smp_a_contrib)));
	if (fnext_contrib == min_contrib) {
		self->_root_cause = 'f';
	} else if (wnext_contrib == min_contrib) {
		self->_root_cause = 'w';
	} else if (smp_r_contrib == min_contrib) {
		self->_root_cause = 'r';
	} else {
		self->_root_cause = 'a';
	}
	return 0;
}

static void default_eval(struct ode *self, dfloat t,
	dfloat smp_t, dfloat smp_r, dfloat smp_a, int *smpc, dfloat **smpv,
	fnl_fd *mf, dfloat *measure, void *mfdata)
{
	void *cvode_mem = CVodeCreate(CV_ADAMS);
	int good, smpb;
//	logfmt(LOG_VERY_VERBOSE, "simulating ode %p for time %g", self, t);

	self->_smp_r = smp_r;
	self->_smp_a = smp_a;

	// context creation
	N_Vector x = N_VMake_OpenMP(self->n, self->state, omp_get_num_threads());
	cvassert(CVodeInit(cvode_mem, cvrhs, 0, x), good);
	cvassert(CVodeSStolerances(cvode_mem, self->rtol, self->atol), good);
	SUNMatrix cvode_jacob = SUNDenseMatrix(self->n, self->n);
	logfmt_assert(cvode_jacob, "failed creating a %d by %d matrix", self->n, self->n);
	SUNLinearSolver cvode_ls = SUNLinSol_Dense(x, cvode_jacob);
	cvassert(CVodeSetLinearSolver(cvode_mem, cvode_ls, cvode_jacob), good);
	cvassert(CVodeSetUserData(cvode_mem, self), good);
	cvassert(CVodeSetMinStep(cvode_mem, 1.0e-13), good);
	cvassert(CVodeSetMaxStep(cvode_mem, self->maxh), good);
	self->_tmpvecn = N_VNew_OpenMP(self->n, omp_get_num_threads());
	if (smpc) {
		self->_smp_last_vec = N_VNew_OpenMP(self->n, omp_get_num_threads());
		if (smp_r != 0 || smp_a != 0 || self->next_w) {
			cvassert(CVodeRootInit(cvode_mem, 1, cvrfp), good);
		}
		if (*smpc) {
			smpb = *smpc;
		} else if (smp_r || smp_a) {
			smpb = 16;
			*smpv = malloc(smpb*self->n*sizeof(dfloat));
			*smpc = 0;
		}
		*smpc = 1;
		smpv[0][0] = 0;
		memcpy(*smpv+1, self->state, self->n*sizeof(dfloat));
	}
	if (self->next_w) {
		self->_wmnext = SUNDenseMatrix(self->n, self->n);
		memcpy(self->wmnext, SM_CONTENT_D(self->_wmnext), self->n*self->n*sizeof(dfloat));
	}
	if (self->tscale) {
	} else if (self->tnext) {
		self->tscale = self->tnext;
	} else {
		self->tscale = t;
	}
	if (smp_r || smp_a || self->next_w || self->next_f) {
		cvassert(CVodeRootInit(cvode_mem, 1, cvrfp), good);
	}

	// actual simulation
	dfloat h, tret;
	bool multistep;
	dfloat tnxtsmp = smp_t;
	dfloat now = 0;
	dfloat mfcur = 0;
	char do_next = 0;
	while (t > now) {
		dfloat tstart = now;
		if (smp_t != 0) {
			h = tnxtsmp - now;
		} else {
			h = t - now;
		}
		if (mf || smp_r != 0 || smp_a != 0) {
			multistep = false;
		} else {
			multistep = true;
		}
		if (self->next_t) {
			cvassert(CVodeSetStopTime(cvode_mem, self->tnext), good);
		}

		good = CVode(cvode_mem, now+h, x, &tret, multistep?CV_NORMAL:CV_ONE_STEP);
		dfloat hlast; int good2;
		cvassert(CVodeGetLastStep(cvode_mem, &hlast), good2);
		if (good == CV_SUCCESS || good == CV_TSTOP_RETURN || good == CV_ROOT_RETURN) { // do nothing
		} else if (good == CV_TOO_CLOSE) {
			break;
		} else if (good == CV_TOO_MUCH_WORK) {
			log_assert(multistep && h == t-now);
			CVodeSetMaxNumSteps(cvode_mem, 1024*8);
			logstr("CVode is complaining about taking too many steps", LOG_WARN);
		} else if (hlast <= 1.0e-13) {
			logstr("CVode set step size too small; trying again with less time", LOG_ERROR);
			do_next = 'h';
			goto cleanup;
		} else {
			logfmt(LOG_ALWAYS, "CVode call failed with code %d\n", good);
			log_unreachable();
		}
		now = tret;

		if (good == CV_TSTOP_RETURN) {
			do_next = 't';
			break;
		}
		if (good == CV_ROOT_RETURN) {
			if (self->_root_cause == 'w') {
				do_next = 'w';
				break;
			} else if (self->_root_cause == 'f') {
				do_next = 'f';
				break;
			} else {
				goto sample;
			}
		}

		int np1;
		if (smp_t && tret >= tnxtsmp) {
sample:
			np1 = self->n + 1;
			(*smpc)++;
			if (*smpc > smpb) {
				smpb *= 2;
				*smpv = realloc(*smpv, smpb*np1*sizeof(dfloat));
			}
			(*smpv+(*smpc-1)*np1)[0] = tret;
//			logfmt(LOG_VERBOSE, "%g: memcpy(*smpv+%d*%d+%d+1)", tret, *smpc-1, np1, 1);
			memcpy(*smpv+(*smpc-1)*np1+1, N_VGetArrayPointer(x), self->n*sizeof(dfloat));
			tnxtsmp = tret + smp_t;
		}

		if (mf) {
			// TODO: something better than the right hand rule
			mfcur += (now-tstart) * mf(self->n, N_VGetArrayPointer(x), mfdata);
//			logfmt(LOG_VERBOSE, "mfcur = %g", mfcur);
		}
	}

cleanup:

	memcpy(self->state, N_VGetArrayPointer(x), self->n*sizeof(dfloat));
	if (mf) *measure = mfcur;

	if (self->next_w) SUNMatDestroy(self->_wmnext);
	N_VDestroy(self->_smp_last_vec);
	N_VDestroy(self->_tmpvecn);
	N_VDestroy(x);
	CVodeFree(&cvode_mem);
	SUNMatDestroy(cvode_jacob);
	cvassert(SUNLinSolFree(cvode_ls), good);

	switch (do_next) {
	case 't':
//		logstr("staging due to time", LOG_VERBOSE);
		memcpy(self->next_t->state, self->state, self->n*sizeof(dfloat));
		self->next_t->eval(self->next_t, t-now, smp_t, smp_r, smp_a, smpc, smpv, mf, measure, mfdata);
		memcpy(self->state, self->next_t->state, self->n*sizeof(dfloat));
		break;
	case 'w':
//		logstr("staging due to weighted norm of state", LOG_VERBOSE);
		memcpy(self->next_w->state, self->state, self->n*sizeof(dfloat));
		self->next_w->eval(self->next_w, t-now, smp_t, smp_r, smp_a, smpc, smpv, mf, measure, mfdata);
		memcpy(self->state, self->next_w->state, self->n*sizeof(dfloat));
		break;
	case 'f':
//		logstr("staging due to function of state", LOG_VERBOSE);
		memcpy(self->next_f->state, self->state, self->n*sizeof(dfloat));
		self->next_f->eval(self->next_f, t-now, smp_t, smp_r, smp_a, smpc, smpv, mf, measure, mfdata);
		memcpy(self->state, self->next_f->state, self->n*sizeof(dfloat));
		break;
	case 'h':
		default_eval(self, tret-now, smp_t, smp_r, smp_a, smpc, smpv, mf, measure, mfdata);
		break;
	}
}

static void no_eval(struct ode *self, dfloat t,
	dfloat smp_t, dfloat smp_r, dfloat smp_a, int *smpc, dfloat **smpv,
	fnl_fd *mf, dfloat *measure, void *mfdata)
{
	return;
}

struct ode ode_skel = {
	.eval = default_eval,
	.rtol = 1.0e-5
};
struct ode ode_done = {
	.eval = no_eval
};
