CFLAGS=-Wall -O2 -g -I. -fopenmp
CC=gcc

LIBS=-lsundials_cvode -lsundials_sunnonlinsolnewton -lsundials_sunlinsoldense -lsundials_sunmatrixdense -lsundials_nvecopenmp -lnlopt -lm -ljansson
OBJFILES=main.o laws.o log.o ode.o optimal-control.o control/directed_thrust.o control/downrange_distance.o control/parse.o control/write.o flight/atmospheric.o flight/inertial_fix.o flight/oneplanet.o flight/parse.o flight/thrust.o

%.o: %.c
	$(CC) -c $(CFLAGS) $< -o $@

redwood: $(OBJFILES)
	$(CC) $(CFLAGS) $(OBJFILES) $(LIBS) -o redwood

clean:
	rm $(OBJFILES)
	rm redwood
